<?php

use \WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;

class Test_ShopSettings extends PHPUnit\Framework\TestCase {

	/**
	 * @var ShopSettings
	 */
	protected $shop_settings_mock;

	/**
	 * Set up.
	 */
	public function setUp(): void {
		\WP_Mock::setUp();

		$this->shop_settings_mock = $this->createMock( ShopSettings::class );
	}

	/**
	 * Tear down.
	 */
	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	/**
	 * Is enabled plugins.
	 */
	public function testIsEnabledPluginsFalse() {
		$this->shop_settings_mock->method( 'is_eu_vat_plugins_enabled' )->willReturn( false );
		$this->assertFalse( $this->shop_settings_mock->is_eu_vat_plugins_enabled() );
	}

}
