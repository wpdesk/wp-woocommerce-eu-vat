<?php

use \WPDesk\WooCommerce\EUVAT\Settings\Settings;

class Test_Settings extends PHPUnit\Framework\TestCase {

	protected $settings;

	/**
	 * Set up.
	 */
	public function setUp(): void {
		\WP_Mock::setUp();

		$this->settings = new Settings();
	}

	/**
	 * Tear down.
	 */
	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	public function testViesValidatePropFalse() {
		$this->assertFalse( $this->settings->eu_vat_vies_validate );
	}

	public function testRemoveB2BPropFalse() {
		$this->assertFalse( $this->settings->eu_vat_remove_vat_from_base_b2b );
	}

}
