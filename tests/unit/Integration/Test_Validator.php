<?php

use Psr\Log\NullLogger;
use WPDesk\Persistence\ElementNotExistsException;
use WPDesk\Persistence\PersistentContainer;
use WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use WPDesk\WooCommerce\EUVAT\Vies\ViesValidator;
use WPDesk\WooCommerce\EUVAT\Vies\Client;


class Test_Validator extends PHPUnit\Framework\TestCase {
	protected $validator;

	/**
	 * Set Up.
	 */
	public function setUp(): void {
		\WP_Mock::setUp();
		if ( ! defined( 'DAY_IN_SECONDS' ) ) {
			define( 'DAY_IN_SECONDS', 60 * 60 * 24 );
		}
		$this->validator = new ViesValidator( $this->getShopSettingsMock(), new NullLogger() );
		$this->validator->set_persistence( $this->getPersistenceMock() );
	}

	/**
	 * Tear down.
	 */
	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	/**
	 * Create ShopSettings mock.
	 *
	 * @return ShopSettings
	 */
	protected function getShopSettingsMock() {
		return $this->createMock( ShopSettings::class );
	}

	/**
	 * Create persistence mock.
	 *
	 * @return PersistentContainer;
	 */
	protected function getPersistenceMock() {
		$mock_builder = $this->getMockBuilder( PersistentContainer::class )
		                     ->getMock();
		$mock_builder->method( 'get' )->willThrowException( new ElementNotExistsException() );

		return $mock_builder;
	}

	/**
	 * Create client mock.
	 *
	 * @return Client
	 */
	protected function getViesClientMock() {
		return new Client();
	}

	/**
	 * @dataProvider getValidVatins
	 */
	public function testValid( $value ) {
		$country    = substr( $value, 0, 2 );
		$vat_number = substr( $value, 2 );
		$this->assertTrue( $this->validator->validate( $vat_number, $country ) );
	}

	/**
	 * @dataProvider getInvalidVatins
	 */
	public function testInvalid( $value ) {
		$country    = substr( $value, 0, 2 );
		$vat_number = substr( $value, 2 );
		$this->assertFalse( $this->validator->validate( $vat_number, $country ) );
	}

	protected function testWrongConnectionThrowsException() {
		$this->expectException( \Psr\Container\ContainerExceptionInterface::class );
		$this->validator->set_client( new Client() );
		$this->validator->validate( '002065538B01', 'NL' );
	}

	/**
	 * @return array
	 */
	public function getValidVatins() {
		return [
			[ 'ATU36890209' ],
		];
	}

	/**
	 * @return array
	 */
	public function getInvalidVatins() {
		return [
			[ 'ATU36890216' ],
		];
	}

}
