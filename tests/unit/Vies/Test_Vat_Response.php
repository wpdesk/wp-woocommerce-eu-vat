<?php

class Test_Vat_Response extends PHPUnit\Framework\TestCase {

	protected $response;

	const COMPANY_NAME = 'WPDesk';
	const COUNTRY_CODE = 'PL';
	const ADDRESS      = 'ul. Stefana 1';
	const VAT          = 'ATU98309332200';

	public function setUp(): void {
		\WP_Mock::setUp();

		$this->response = new CheckVatResponse( true );
		$this->response->setValid( true );
		$this->response->setName( self::COMPANY_NAME );
		$this->response->setCountryCode( self::COUNTRY_CODE );
		$this->response->setAddress( self::ADDRESS );
		$this->response->setVatNumber( self::VAT );
	}

	/**
	 * Tear down.
	 */
	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	public function testIsValidEqualsTrue() {
		$this->assertTrue( $this->response->isValid() );
	}

	public function testIsNameEquals() {
		$this->assertEquals( $this->response->getName(), self::COMPANY_NAME );
	}

	public function testIsCountryCodeEquals() {
		$this->assertEquals( $this->response->getCountryCode(), self::COUNTRY_CODE );
	}

	public function testIsAddressEquals() {
		$this->assertEquals( $this->response->getAddress(), self::ADDRESS );
	}

	public function testIsVatNumberEquals() {
		$this->assertEquals( $this->response->getVatNumber(), self::VAT );
	}

}
