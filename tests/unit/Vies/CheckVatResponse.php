<?php

use WPDesk\WooCommerce\EUVAT\Vies\Response as BaseClass;

class CheckVatResponse extends BaseClass {

	public function __construct( $valid ) {
		$this->valid = $valid;
	}

	/**
	 * Set valid.
	 *
	 * @param bool $valid Is valid.
	 */
	public function setValid( $valid ) {
		$this->valid = $valid;
	}

	/**
	 * Get country code.
	 *
	 * @param string $country_code County ISO.
	 */
	public function setCountryCode( $country_code ) {
		$this->countryCode = $country_code;
	}

	/**
	 * Set VAT number.
	 *
	 * @param string $vat_number Vat number.
	 */
	public function setVatNumber( $vat_number ) {
		$this->vatNumber = $vat_number;
	}

	/**
	 * Set name.
	 *
	 * @param string $name Name.
	 */
	public function setName( $name ) {
		$this->name = $name;
	}

	/**
	 * Get address.
	 *
	 * @param string $address Address.
	 */
	public function setAddress( $address ) {
		$this->address = $address;
	}
}
