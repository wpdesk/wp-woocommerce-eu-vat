<?php

use WPDesk\WooCommerce\EUVAT\Vies\Client;
use WPDesk\WooCommerce\EUVAT\Vies\Response;

class Test_Client extends PHPUnit\Framework\TestCase {

	/**
	 * Set Up.
	 */
	public function setUp(): void {
		\WP_Mock::setUp();
	}

	/**
	 * Tear down.
	 */
	public function tearDown(): void {
		\WP_Mock::tearDown();
	}

	/**
	 * Check real VAT number. ATOMIC SKI.
	 * @dataProvider getRealEuVatCompanies
	 *
	 * @throws \Exception
	 */
	public function testCheckRealVat( $vat, $country, $name, $address ) {
		$client   = new Client( 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl' );
		$response = $client->checkVat( $country, $vat );
		$this->assertInstanceOf( Response::class, $response );
		$this->assertTrue( $response->isValid() );
		$this->assertEquals( $country, $response->getCountryCode(), 'Country is not valid' );
		$this->assertEquals( $vat, $response->getVatNumber(), 'VAT is not valid' );
		$this->assertInstanceOf( '\DateTime', $response->getRequestDate() );
		$this->assertEquals( $name, $response->getName(), 'Name is not valid' );
		$this->assertEquals( $address, $response->getAddress(), 'Address is not valid' );
	}

	/**
	 * Check fake VAT number.
	 *
	 * @dataProvider getFakeEuVatCompanies
	 * @throws \Exception
	 */
	public function testCheckFakeVatNumber( $vat, $country, $name, $address ) {
		$client   = new Client( 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl' );
		$response = $client->checkVat( $country, $vat );

		$this->assertInstanceOf( Response::class, $response );
		$this->assertFalse( $response->isValid() );
		$this->assertEquals( $country, $response->getCountryCode() );
		$this->assertEquals( $vat, $response->getVatNumber(), 'VAT is not valid' );
		$this->assertInstanceOf( '\DateTime', $response->getRequestDate() );
		$this->assertEquals( $name, $response->getName(), 'Name is not valid' );
		$this->assertEquals( $address, $response->getAddress(), 'Address is not valid' );
	}


	/**
	 * @return array
	 */
	public function getRealEuVatCompanies() {
		return array(
			[
				'vat'     => 'U36890209',
				'country' => 'AT',
				'name'    => 'ATOMIC Austria GmbH',
				'address' => "Atomic Straße 1\nAT-5541 Altenmarkt im Pongau",
			],
		);
	}


	/**
	 * @return array
	 */
	public function getFakeEuVatCompanies() {
		return array(
			[
				'vat'     => 'U3689021612',
				'country' => 'AT',
				'name'    => '---',
				'address' => "---",
			],
		);
	}
}
