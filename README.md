[![pipeline status](https://gitlab.com/wpdesk/wp-woocommerce-eu-vat/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-woocommerce-eu-vat/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-woocommerce-eu-vat/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-woocommerce-eu-vat/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-woocommerce-eu-vat/v/stable)](https://packagist.org/packages/wpdesk/wp-woocommerce-eu-vat) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-woocommerce-eu-vat/downloads)](https://packagist.org/packages/wpdesk/wp-woocommerce-eu-vat) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-woocommerce-eu-vat/v/unstable)](https://packagist.org/packages/wpdesk/wp-woocommerce-eu-vat) 
[![License](https://poser.pugx.org/wpdesk/wp-woocommerce-eu-vat/license)](https://packagist.org/packages/wpdesk/wp-woocommerce-eu-vat) 


WooCommerce EU VAT library
===================================================
The library is used to exempt VAT for digital good for European Union customers.

## Requirements

PHP 5.5 or later.