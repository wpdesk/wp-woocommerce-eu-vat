<?php

namespace WPDesk\WooCommerce\EUVAT;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use WPDesk\View\Renderer\Renderer;
use WPDesk\WooCommerce\EUVAT\Interfaces\ValidatorInterface;
use WPDesk\WooCommerce\EUVAT\Settings\Settings;
use WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use WPDesk\WooCommerce\EUVAT\Vies\ViesOrderMetaBox;
use WPDesk\WooCommerce\EUVAT\Vies\ViesValidator;
use WPDesk\WooCommerce\EUVAT\Integration\Checkout;
use WPDesk\WooCommerce\EUVAT\Integration\MetaData;
use WPDesk\WooCommerce\EUVAT\Integration\Notices;
use WPDesk\PluginBuilder\Plugin\HookableCollection;
use WPDesk\PluginBuilder\Plugin\HookableParent;
use WPDesk\View\Renderer\SimplePhpRenderer;
use WPDesk\View\Resolver\DirResolver;
use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * EU Vat Integration.
 *
 * @package WPDesk\Integration\EUVAT
 */
class WooCommerceEUVAT implements Hookable, HookableCollection, LoggerAwareInterface {

	use HookableParent;
	use LoggerAwareTrait;

	/**
	 * @var Settings
	 */
	protected $settings;

	/**
	 * @var ShopSettings
	 */
	protected $shop_settings;

	/**
	 * @var LoggerAwareInterface
	 */
	protected $logger;

	/**
	 * @var Renderer
	 */
	protected $renderer;

	/**
	 * @var ValidatorInterface
	 */
	private $validator;

	const SCRIPTS_VERSION = 1;

	/**
	 * @var string
	 */
	private $vat_field_name = 'vat_number';

	/**
	 * @var string[]
	 */
	private $plugin_data = [
		'plugin_name'    => '',
		'plugin_version' => '',
		'doc_link'       => '',
	];

	/**
	 * @param Settings        $settings Settings.
	 * @param LoggerInterface $logger   Logger.
	 */
	public function __construct( Settings $settings, LoggerInterface $logger ) {
		$this->settings      = $settings;
		$this->shop_settings = $this->get_shop_settings();
		$this->set_validator( new ViesValidator( $this->shop_settings, $logger ) );
		$this->set_logger( $logger );
		$this->set_renderer( new SimplePhpRenderer( new DirResolver( __DIR__ . '/Integration/Views' ) ) );
	}

	/**
	 * @param string $plugin_name
	 * @param string $plugin_version
	 * @param string $doc_link
	 *
	 * @return void
	 */
	public function set_plugin_data( string $plugin_name, string $plugin_version, string $doc_link ) {
		$this->plugin_data = [
			'plugin_name'    => $plugin_name,
			'plugin_version' => $plugin_version,
			'doc_link'       => $doc_link,
		];
	}

	/**
	 * @return array
	 */
	public function get_plugin_data(): array {
		return $this->plugin_data;
	}

	/**
	 * @param string $vat_field_name Vat number field name.
	 */
	public function set_vat_field_name( string $vat_field_name ) {
		$this->vat_field_name = $vat_field_name;
	}

	/**
	 * @return string
	 */
	public function get_vat_field_name(): string {
		return $this->vat_field_name;
	}

	/**
	 * @param LoggerInterface $logger
	 */
	public function set_logger( LoggerInterface $logger ) {
		$this->logger = $logger;
	}

	/**
	 * @param Renderer $renderer Renderer.
	 */
	public function set_renderer( Renderer $renderer ) {
		$this->renderer = $renderer;
	}

	/**
	 * @param ValidatorInterface $validator Validator.
	 */
	public function set_validator( ValidatorInterface $validator ) {
		$this->validator = $validator;
	}

	/**
	 * @return ValidatorInterface
	 */
	public function get_validator(): ValidatorInterface {
		return $this->validator;
	}

	/**
	 * @return ShopSettings
	 */
	public function get_shop_settings(): ShopSettings {
		return new ShopSettings();
	}

	/**
	 * @return bool
	 */
	public function is_eu_vat_plugins_enabled(): bool {
		return $this->shop_settings->is_eu_vat_plugins_enabled();
	}

	/**
	 * Load dependencies.
	 */
	public function load_dependencies() {
		$this->add_hookable( new Notices( $this->is_eu_vat_plugins_enabled(), $this->get_plugin_data() ) );
		if ( $this->settings->eu_vat_vies_validate && ! $this->is_eu_vat_plugins_enabled() && wc_tax_enabled() ) {
			$this->add_hookable( new Checkout( $this->get_vat_field_name(), $this->shop_settings, $this->settings, $this->renderer, $this->get_validator() ) );
			$this->add_hookable( new MetaData( $this->shop_settings ) );
			$this->add_hookable( new ViesOrderMetaBox( $this->settings, $this->shop_settings, $this->renderer ) );
		}
	}

	/**
	 * Fires hooks.
	 */
	public function hooks() {
		$this->load_dependencies();
		$this->hooks_on_hookable_objects();
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ], 30 );
	}

	/**
	 * Admin enqueue scripts.
	 */
	public function admin_enqueue_scripts() {
		wp_enqueue_style(
			'euvat',
			trailingslashit( plugin_dir_url( __DIR__ ) ) . 'src/assets/css/admin.css',
			'',
			self::SCRIPTS_VERSION
		);
	}

}
