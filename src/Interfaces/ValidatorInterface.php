<?php
/**
 * Validator interfaces.
 *
 * @package WPDesk\Integration\EUVAT.
 */

namespace WPDesk\WooCommerce\EUVAT\Interfaces;

use WPDesk\WooCommerce\EUVAT\Vies\Exceptions\ViesCommunicationException;

/**
 * Validates values.
 */
interface ValidatorInterface {

	/**
	 * Is valid value.
	 *
	 * @param string $vat_number Vat number.
	 * @param string $country    Country ISO.
	 *
	 * @return array
	 * @throws ViesCommunicationException
	 */
	public function validate( $vat_number, $country );

}
