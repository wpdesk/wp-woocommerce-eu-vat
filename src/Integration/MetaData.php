<?php

namespace WPDesk\WooCommerce\EUVAT\Integration;

use WC_Order;
use WC_Order_Refund;
use WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Save order metadata.
 *
 * @package WPDesk\WooCommerce\EUVAT\Integration
 */
class MetaData implements Hookable {

	/**
	 * @var ShopSettings
	 */
	private $shop_settings;

	/**
	 * @param string       $vat_field_name
	 * @param ShopSettings $shop_settings
	 */
	public function __construct( ShopSettings $shop_settings ) {
		$this->shop_settings  = $shop_settings;
	}

	/**
	 * Fires hooks.
	 */
	public function hooks() {
		add_action( 'woocommerce_checkout_create_order', [ $this, 'save_order_data_action' ] );
		add_action( 'woocommerce_create_refund', [ $this, 'save_refund_data_action' ] );
	}

	/**
	 * Save VAT Number to the order during checkout (WC 2.7.x).
	 *
	 * @param WC_Order $order Order.
	 *
	 * @return void
	 */
	public function save_order_data_action( WC_Order $order ) {
		$data          = wp_unslash( $_POST );
		$vies_is_valid = WC()->session->get( 'vies_is_valid' );
		$vat_number    = WC()->session->get( 'vies_vat_number' );
		if ( $vat_number && ! is_null( $vies_is_valid ) ) {
			$order->update_meta_data( '_vat_number', $vat_number );
			$order->update_meta_data( '_vat_number_self_declared', isset( $data['vat_confirmation'] ) ? 'true' : 'false' );
			$order->update_meta_data( '_vat_number_is_validated', 'true' );
			$order->update_meta_data( '_vat_number_is_valid', $vies_is_valid ? 'true' : 'false' );
		}

		if ( $this->shop_settings->get_ip_country() === $data['country'] || isset( $data['location_confirmation'] ) ) {
			$order->update_meta_data( '_customer_ip_country', $this->shop_settings->get_ip_country() );
			$order->update_meta_data( '_customer_self_declared_country', isset( $data['location_confirmation'] ) ? 'true' : 'false' );
		}
	}

	/**
	 * Save VAT Number to the customer during checkout (WC 2.7.x).
	 *
	 * @param WC_Order_Refund $refund Refund.
	 */
	public function save_refund_data_action( WC_Order_Refund $refund ) {
		$order = wc_get_order( $refund->get_parent_id() );
		$refund->update_meta_data( '_vat_number', $order->get_meta( '_vat_number' ) );
	}

}
