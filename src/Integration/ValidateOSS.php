<?php

namespace WPDesk\WooCommerce\EUVAT\Integration;

use Exception;
use WC_Geolocation;
use WC_Product;
use WPDesk\WooCommerce\EUVAT\Helper\Normalizer;
use WPDesk\WooCommerce\EUVAT\Interfaces\ValidatorInterface;
use WPDesk\WooCommerce\EUVAT\Settings\Settings;
use WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use WPDesk\WooCommerce\EUVAT\Vies\Exceptions\ViesCommunicationException;
use WPDesk\WooCommerce\EUVAT\Vies\ViesValidator;

/**
 * @package WPDesk\WooCommerce\EUVAT\Integration
 */
class ValidateOSS {
	/**
	 * Library settings.
	 *
	 * @var Settings
	 */
	private $settings;
	/**
	 * Shop settings.
	 *
	 * @var ShopSettings
	 */
	private $shop_settings;
	/**
	 * Vies validation.
	 *
	 * @var ValidatorInterface
	 */
	private $validator;

	public function __construct( ShopSettings $shop_settings, Settings $settings, ViesValidator $validator ) {
		$this->settings      = $settings;
		$this->shop_settings = $shop_settings;
		$this->validator     = $validator;
	}

	public function get_shop_settings(): ShopSettings {
		return $this->shop_settings;
	}

	/**
	 * Set tax exception based on countries.
	 *
	 * @param string $billing_country  Billing country of customer.
	 * @param string $shipping_country Shipping country of customer.
	 */
	public function set_vat_exempt( string $billing_country, string $shipping_country ) {
		if ( 'billing' === get_option( 'woocommerce_tax_based_on', 'billing' ) ) {
			$base_country_match = WC()->countries->get_base_country() === $billing_country;
		} else {
			$base_country_match = in_array( WC()->countries->get_base_country(), [ $billing_country, $shipping_country ] );
		}
		if ( $base_country_match && $this->settings->eu_vat_remove_vat_from_base_b2b || ! $base_country_match ) {
			WC()->customer->set_is_vat_exempt( true );
		}
	}

	/**
	 * Can exempt VAT for B2B transactions.
	 *
	 * @param $vat_field
	 * @param $vat_confirmation
	 * @param $billing_country
	 * @param $shipping_country
	 * @param $ship_to_different_address
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function should_exempt_vat_for_b2b( $vat_field, $vat_confirmation, $billing_country, $shipping_country, $ship_to_different_address ): bool {
		$vat_eu_confirmed_by_user = $vat_confirmation;
		$normalizer               = new Normalizer();
		$billing_country          = $normalizer->normalize_country_prefix( $billing_country );
		$shipping_country         = ! empty( $shipping_country ) && ! empty( $ship_to_different_address ) ? $shipping_country : $billing_country;
		$shipping_country         = $normalizer->normalize_country_prefix( $shipping_country );
		$vat_number               = $normalizer->normalize_vat_number( $vat_field, $billing_country );
		if ( in_array( $billing_country, $this->shop_settings->get_eu_countries() ) && ! empty( $vat_number ) ) {
			try {
				$is_valid = $this->validator->validate( $vat_number, $billing_country );
				WC()->session->set( 'vies_is_valid', $is_valid );
				WC()->session->set( 'vies_vat_number', $vat_number );
				if ( $is_valid || $vat_eu_confirmed_by_user ) {
					$this->set_vat_exempt( $billing_country, $shipping_country );

					return true;
				} else {
					$fail_handler = $this->settings->eu_vat_failure_handling;
					switch ( $fail_handler ) {
						case 'accept_with_vat':
							break;
						case 'accept_without_vat':
							$this->set_vat_exempt( $billing_country, $shipping_country );
							break;
						default:
							throw new Exception( sprintf( __( 'You have entered an invalid VAT number (%1$s) for your billing country (%2$s).', 'wp-woocommerce-eu-vat' ), $vat_number, $billing_country ), '400' );
					}
				}
			} catch ( ViesCommunicationException $e ) {
				throw new Exception( $e->getMessage(), '400' );
			}
		}

		return false;
	}

	/**
	 * @param array $post_arr Post data.
	 *
	 * @throws Exception
	 */
	public function should_allow_oss_transaction_for_b2c( array $post_arr ) {
		if ( $this->settings->moss_validate_ip && $this->is_valid_for_oss( $post_arr['billing_country'] ) ) {
			if ( $this->is_self_declaration_required( $this->shop_settings->get_ip_country(), $post_arr['billing_country'] ) && empty( $post_arr['location_confirmation'] ) ) {
				// translators: %1$s ip address, %2$s country.
				throw new Exception( sprintf( __( 'Your IP Address (%1$s) does not match your billing country (%2$s). European VAT laws require your IP address to match your billing country when purchasing digital goods in the EU. Please confirm you are located within your billing country using the checkbox below.', 'wp-woocommerce-eu-vat' ), apply_filters( 'wc_eu_vat_self_declared_ip_address', WC_Geolocation::get_ip_address() ), esc_attr( $post_arr['billing_country'] ) ), 'error' );
			}
			WC()->session->set( 'vies_is_valid', '' );
			WC()->session->set( 'vies_vat_number', '' );
		}
	}

	/**
	 * See if we need the user to self-declare location.
	 *
	 * @param string|null $ip_country      IP country.
	 * @param string|null $billing_country Billing country.
	 *
	 * @return bool
	 */
	public function is_self_declaration_required( string $ip_country = null, string $billing_country = null ): bool {
		if ( is_null( $ip_country ) ) {
			$ip_country = $this->shop_settings->get_ip_country();
		}
		if ( is_null( $billing_country ) ) {
			$billing_country = $this->get_billing_country();
		}

		return ( empty( $ip_country ) || in_array( $ip_country, $this->shop_settings->get_eu_countries(), true ) || in_array( $billing_country, $this->shop_settings->get_eu_countries(), true ) ) && $ip_country !== $billing_country;
	}

	/**
	 * @return string
	 */
	public function get_billing_country(): string {
		return is_callable( [ WC()->customer, 'get_billing_country' ] ) ? WC()->customer->get_billing_country() : WC()->customer->get_country();
	}


	/**
	 * Check is oss product and customer country is out of country base.
	 *
	 * @param string $country Country slug.
	 *
	 * @return bool
	 */
	public function is_valid_for_oss( string $country ): bool {
		return $this->product_has_oss_tax_class() && in_array( $country, $this->shop_settings->get_eu_countries() ) && ! $this->shop_settings->is_customer_from_base_country( $country );
	}

	/**
	 * Sees if a product has a OSS tax class.
	 *
	 * @return bool
	 */
	public function product_has_oss_tax_class(): bool {
		$has_tax_class = false;
		if ( WC()->cart->get_cart() ) {
			foreach ( WC()->cart->get_cart() as $values ) {
				/**
				 * Product.
				 *
				 * @var WC_Product $_product
				 */
				$_product  = $values['data'];
				$tax_class = $_product->get_tax_class();
				if ( '' === $tax_class ) {
					$tax_class = 'standard';
				}
				if ( in_array( $tax_class, $this->settings->moss_tax_classes ) ) {
					$has_tax_class = true;
				}
			}
		}

		return $has_tax_class;
	}

}
