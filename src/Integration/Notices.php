<?php

namespace WPDesk\WooCommerce\EUVAT\Integration;

use WPDesk\Notice\Notice;
use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Adds notifications for the administrator about plugin activation and rounding taxes at subtotal.
 *
 * @package WPDesk\WooCommerce\EUVAT\Integration
 */
class Notices implements Hookable {

	/**
	 * Is EU VAT Number plugins is enabled.
	 *
	 * @var bool
	 */
	private $is_eu_vat_plugins_enabled;

	/**
	 * @var array
	 */
	private $plugin_data;

	/**
	 * Notices constructor.
	 *
	 * @param bool  $is_eu_vat_plugins_enabled Is EU VAT Number plugins is enabled.
	 * @param array $plugin_data               Plugin data.
	 */
	public function __construct( bool $is_eu_vat_plugins_enabled, array $plugin_data ) {
		$this->is_eu_vat_plugins_enabled = $is_eu_vat_plugins_enabled;
		$this->plugin_data               = $plugin_data;
	}

	/**
	 * Fires hooks.
	 */
	public function hooks() {
		if ( $this->is_eu_vat_plugins_enabled ) {
			add_action( 'admin_init', [ $this, 'deactivate_eu_vat_notice_action' ] );
		} else {
			if ( 'no' === get_option( 'woocommerce_tax_round_at_subtotal', 'no' ) ) {
				add_action( 'admin_init', [ $this, 'round_taxes_at_subtotal_level_notice_action' ], 12 );
			}
		}
	}

	/**
	 * Show notice about disabling Integration EU VAT Number.
	 */
	public function deactivate_eu_vat_notice_action() {
		// translators: %1$s - plugin name, %2$s - plugin version, %3$s - docs link about moss.
		new Notice( sprintf( __( '<strong>%1$s plugin from version %2$s supports MOSS.</strong> <a href="%3$s" target="_blank">Here</a> you can read how to configure it. You no longer need any other plugins for invoicing with MOSS standard. Please deactivate them.', 'wp-woocommerce-eu-vat' ), $this->plugin_data['plugin_name'], $this->plugin_data['plugin_version'], $this->plugin_data['doc_link'] ), 'warning', true );
	}

	/**
	 * Show notice about rounding taxes at subtotal level.
	 */
	public function round_taxes_at_subtotal_level_notice_action() {
		// translators: settings link.
		new Notice( sprintf( __( 'Tax rounding at subtotals is off. Please check "Rounding" option for invoice plugin to function properly. To find it click <a href="%s">here</a> or go to Integration tax settings.', 'wp-woocommerce-eu-vat' ), admin_url( 'admin.php?page=wc-settings&tab=tax' ) ), 'error', true );
	}

}
