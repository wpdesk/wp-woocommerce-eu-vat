<?php

namespace WPDesk\WooCommerce\EUVAT\Integration;

use \WC_Order;
use WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use WPDesk\PluginBuilder\Plugin\Hookable;

/**
 * Format some data saved to order & localization.
 *
 * @package WPDesk\WooCommerce\EUVAT\Integration
 */
class Formatter implements Hookable {

	/**
	 * @var ShopSettings
	 */
	private $shop_settings;

	/**
	 * @param ShopSettings $shop_settings Shop settings.
	 */
	public function __construct( ShopSettings $shop_settings ) {
		$this->shop_settings = $shop_settings;
	}

	/**
	 * Fires hooks.
	 */
	public function hooks() {
		add_filter( 'woocommerce_order_formatted_billing_address', [ $this, 'formatted_billing_address_action' ], 10, 2 );
		add_filter( 'woocommerce_formatted_address_replacements', [ $this, 'output_company_vat_number_action' ], 10, 2 );
		add_filter( 'woocommerce_localisation_address_formats', [ $this, 'localisation_address_formats_action' ], 10, 2 );
	}

	/**
	 * @param array    $address Address.
	 * @param WC_Order $order   Order.
	 *
	 * @return array
	 */
	public function formatted_billing_address_action( array $address, WC_Order $order ): array {
		$vat_id = $order->get_meta( '_vat_number' );
		if ( $vat_id ) {
			$address['vat_id'] = $vat_id;
		}

		return $address;
	}

	/**
	 * Add {vat_id} placeholder.
	 *
	 * @param array $formats Formats.
	 * @param array $args    Arguments.
	 *
	 * @return array
	 */
	public function output_company_vat_number_action( array $formats, array $args ): array {
		if ( isset( $args['vat_id'] ) ) {
			// Translators: vat type like 20%.
			$formats['{vat_id}'] = sprintf( __( 'VAT ID: %s', 'wp-woocommerce-eu-vat' ), $args['vat_id'] );
		} else {
			$formats['{vat_id}'] = '';
		}

		return $formats;
	}

	/**
	 * Address formats.
	 *
	 * @param array $formats Formats.
	 *
	 * @return array
	 */
	public function localisation_address_formats_action( array $formats ): array {
		foreach ( $formats as $key => $format ) {
			if ( 'default' === $key || in_array( $key, $this->shop_settings->get_eu_countries() ) ) {
				$formats[ $key ] .= "\n{vat_id}";
			}
		}

		return $formats;
	}

}
