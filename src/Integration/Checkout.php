<?php

namespace WPDesk\WooCommerce\EUVAT\Integration;

use WC_Geolocation;
use WC_Product;
use WPDesk\WooCommerce\EUVAT\Helper\Normalizer;
use WPDesk\WooCommerce\EUVAT\Interfaces\ValidatorInterface;
use WPDesk\WooCommerce\EUVAT\Settings\Settings;
use WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use WPDesk\WooCommerce\EUVAT\Vies\Exceptions\ViesCommunicationException;
use WPDesk\WooCommerce\EUVAT\Vies\ViesValidator;
use WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\View\Renderer\Renderer;

/**
 * Checkout functionality.
 *
 * @package WPDesk\WooCommerce\EUVAT\Integration
 */
class Checkout implements Hookable {

	/**
	 * Library settings.
	 *
	 * @var Settings
	 */
	private $settings;

	/**
	 * Shop settings.
	 *
	 * @var ShopSettings
	 */
	private $shop_settings;

	/**
	 * Vies validation.
	 *
	 * @var ValidatorInterface
	 */
	private $validator;

	/**
	 * Renderer.
	 *
	 * @var Renderer
	 */
	private $renderer;

	/**
	 * Validation results.
	 *
	 * @var array
	 */
	private $is_valid = false;

	/**
	 * @var string
	 */
	private $vat_field_name;

	public function __construct( string $vat_field_name, ShopSettings $shop_settings, Settings $settings, Renderer $renderer, ViesValidator $validator ) {
		$this->vat_field_name = $vat_field_name;
		$this->settings       = $settings;
		$this->shop_settings  = $shop_settings;
		$this->renderer       = $renderer;
		$this->validator      = $validator;
	}

	/**
	 * Fires hooks.
	 */
	public function hooks() {
		add_action( 'woocommerce_checkout_process', [ $this, 'process_checkout_action' ] );
		add_action( 'woocommerce_checkout_update_order_review', [ $this, 'ajax_update_checkout_totals_action' ] );
		add_action( 'woocommerce_review_order_before_submit', [ $this, 'location_confirmation_action' ] );
		add_action( 'woocommerce_review_order_before_submit', [ $this, 'vat_confirmation_action' ] );
		add_filter( 'woocommerce_get_tax_location', [ $this, 'woocommerce_get_tax_location' ], 10, 2 );
		add_filter( 'woocommerce_checkout_fields', [ $this, 'add_required_classes' ], 20 );
	}

	/**
	 * Set tax exception based on countries.
	 *
	 * @param string $billing_country  Billing country of customer.
	 * @param string $shipping_country Shipping country of customer.
	 */
	public function set_vat_exempt( string $billing_country, string $shipping_country ) {
		if ( 'billing' === get_option( 'woocommerce_tax_based_on', 'billing' ) ) {
			$base_country_match = ( WC()->countries->get_base_country() === $billing_country );
		} else {
			$base_country_match = in_array( WC()->countries->get_base_country(), [
				$billing_country,
				$shipping_country,
			] );
		}

		if ( ( $base_country_match && $this->settings->eu_vat_remove_vat_from_base_b2b ) || ! $base_country_match ) {
			WC()->customer->set_is_vat_exempt( true );
		}
	}

	/**
	 * Reset VAT exempt.
	 */
	public function reset() {
		WC()->customer->set_is_vat_exempt( false );
	}

	/**
	 * Validate the VAT number when the checkout form is processed.
	 * For B2C transactions, validate the IP only if this is a digital order.
	 */
	public function process_checkout_action() {
		$form_data = wc_clean( $_POST ); //phpcs:ignore
		$this->maybe_vat_exempt( $form_data );
	}

	/**
	 * Triggered when the totals are updated on the checkout.
	 *
	 * @param string $data Data.
	 *
	 * @return void
	 */
	public function ajax_update_checkout_totals_action( string $data ) {
		parse_str( $data, $form_data );
		$this->maybe_vat_exempt( $form_data );
	}

	/**
	 * Maybe vat exempt.
	 *
	 * @param $form_data
	 *
	 * @return void
	 */
	private function maybe_vat_exempt( $form_data ): void {
		$this->reset();

		if ( ! WC()->cart->needs_payment() || ! $this->product_has_oss_tax_class() ) {
			return;
		}

		if ( empty( $form_data['billing_country'] ) && empty( $form_data['shipping_country'] ) ) {
			return;
		}

		if ( $this->shop_settings->is_customer_from_base_country( $form_data['billing_country'] ) ) {
			return;
		}

		if ( ! empty( $form_data[ $this->vat_field_name ] ) ) {
			$this->should_exempt_vat_for_b2b( $form_data );
		} else {
			$this->should_allow_oss_transaction_for_b2c( $form_data );
		}

	}

	/**
	 * Can exempt VAT for B2B transactions.
	 *
	 * @param array $data Post data.
	 */
	public function should_exempt_vat_for_b2b( array $data ) {
		$vat_eu_confirmed_by_user = isset( $data['vat_confirmation'] );
		$normalizer               = new Normalizer();
		$billing_country          = $normalizer->normalize_country_prefix( $data['billing_country'] );
		$shipping_country         = ! empty( $data['shipping_country'] ) && ! empty( $data['ship_to_different_address'] ) ? $data['shipping_country'] : $data['billing_country'];
		$shipping_country         = $normalizer->normalize_country_prefix( $shipping_country );
		$vat_number               = $normalizer->normalize_vat_number( $data[ $this->vat_field_name ], $billing_country );
		if ( in_array( $billing_country, $this->shop_settings->get_eu_countries() ) && ! empty( $vat_number ) ) {
			try {
				$this->is_valid = $this->validator->validate( $vat_number, $billing_country );
				WC()->session->set( 'vies_is_valid', $this->is_valid );
				WC()->session->set( 'vies_vat_number', $vat_number );
				if ( $this->is_valid || $vat_eu_confirmed_by_user ) {
					$this->set_vat_exempt( $billing_country, $shipping_country );
				} else {
					$fail_handler = $this->settings->eu_vat_failure_handling;
					switch ( $fail_handler ) {
						case 'accept_with_vat':
							break;
						case 'accept_without_vat':
							$this->set_vat_exempt( $billing_country, $shipping_country );
							break;
						default:
							wc_add_notice( sprintf( __( 'You have entered an invalid VAT number (%1$s) for your billing country (%2$s).', 'wp-woocommerce-eu-vat' ), $vat_number, $billing_country ), 'error' );
							break;
					}
				}
			} catch ( ViesCommunicationException $e ) {
				wc_add_notice( $e->getMessage(), 'error' );
			}

		}
	}

	/**
	 * @param array $post_arr Post data.
	 */
	public function should_allow_oss_transaction_for_b2c( array $post_arr ) {
		if ( $this->settings->moss_validate_ip && $this->is_valid_for_oss( $post_arr['billing_country'] ) ) {
			if ( $this->is_self_declaration_required( $this->shop_settings->get_ip_country(), $post_arr['billing_country'] ) && empty( $post_arr['location_confirmation'] ) ) {
				// translators: %1$s ip address, %2$s country.
				wc_add_notice( sprintf( __( 'Your IP Address (%1$s) does not match your billing country (%2$s). European VAT laws require your IP address to match your billing country when purchasing digital goods in the EU. Please confirm you are located within your billing country using the checkbox below.', 'wp-woocommerce-eu-vat' ), apply_filters( 'wc_eu_vat_self_declared_ip_address', WC_Geolocation::get_ip_address() ), esc_attr( $post_arr['billing_country'] ) ), 'error' );
			}
			WC()->session->set( 'vies_is_valid', '' );
			WC()->session->set( 'vies_vat_number', '' );
		}
	}

	/**
	 * See if we need the user to self-declare location.
	 *
	 * @param string|null $ip_country      IP country.
	 * @param string|null $billing_country Billing country.
	 *
	 * @return bool
	 */
	public function is_self_declaration_required( string $ip_country = null, string $billing_country = null ): bool {
		if ( is_null( $ip_country ) ) {
			$ip_country = $this->shop_settings->get_ip_country();
		}
		if ( is_null( $billing_country ) ) {
			$billing_country = $this->get_billing_country();
		}

		return ( empty( $ip_country ) || in_array( $ip_country, $this->shop_settings->get_eu_countries(), true ) || in_array( $billing_country, $this->shop_settings->get_eu_countries(), true ) ) && $ip_country !== $billing_country;
	}

	/**
	 * Show checkbox for customer to confirm their location if geolocation fail.
	 */
	public function location_confirmation_action(): string {
		$form_data = [];
		if ( isset( $_POST['post_data'] ) ) {
			parse_str( $_POST['post_data'], $form_data );
		}

		$country = $form_data['billing_country'] ?? '';
		if ( $this->settings->moss_validate_ip && $this->is_valid_for_oss( $country ) ) {
			if ( ! $this->is_valid && $this->is_self_declaration_required() && empty( $form_data[ $this->vat_field_name ] ) ) {
				$params['is_checked'] = isset( $form_data['location_confirmation'] );
				$params['countries']  = WC()->countries->get_countries();
				$html                 = $this->renderer->render( 'location-confirmation-field', $params );
				echo $html; //phpcs:ignore
			}
		}

		return '';
	}

	/**
	 * @return string
	 */
	private function get_billing_country(): string {
		return is_callable( [ WC()->customer, 'get_billing_country' ] ) ? WC()->customer->get_billing_country() : WC()->customer->get_country();
	}

	/**
	 * Show the checkbox for the customer to confirm the EU VAT number.
	 * For those companies where the EU has not added a number to the VIES database.
	 */
	public function vat_confirmation_action(): void {
		$form_data = [];
		if ( isset( $_POST['post_data'] ) ) {
			parse_str( $_POST['post_data'], $form_data );
		}
		$country = $form_data['billing_country'] ?? '';
		if ( ! empty( $form_data[ $this->vat_field_name ] ) && $this->settings->moss_validate_ip && $this->is_valid_for_oss( $country ) ) {
			if ( 'reject' === $this->settings->eu_vat_failure_handling && false === $this->is_valid && $this->settings->moss_validate_ip && in_array( $this->get_billing_country(), $this->shop_settings->get_eu_countries(), true ) ) {
				$params['is_checked'] = isset( $form_data['vat_confirmation'] ); //phpcs:ignore
				$params['countries']  = WC()->countries->get_countries();
				$html                 = $this->renderer->render( 'vat-confirmation-field', $params );
				echo $html; //phpcs:ignore
			}
		}
	}

	/**
	 * Check is oss product and customer country is out of country base.
	 *
	 * @param string $country Country slug.
	 *
	 * @return bool
	 */
	private function is_valid_for_oss( string $country ): bool {
		return $this->product_has_oss_tax_class() && in_array( $country, $this->shop_settings->get_eu_countries() ) && ! $this->shop_settings->is_customer_from_base_country( $country );
	}

	/**
	 * Sees if a product has a OSS tax class.
	 *
	 * @return bool
	 */
	private function product_has_oss_tax_class(): bool {
		$has_tax_class = false;

		if ( WC()->cart->get_cart() ) {
			foreach ( WC()->cart->get_cart() as $values ) {
				/**
				 * Product.
				 *
				 * @var WC_Product $_product
				 */
				$_product  = $values['data'];
				$tax_class = $_product->get_tax_class();
				if ( '' === $tax_class ) {
					$tax_class = 'standard';
				}
				if ( in_array( $tax_class, $this->settings->moss_tax_classes ) ) {
					$has_tax_class = true;
				}
			}
		}

		return $has_tax_class;
	}


	/**
	 * Force Digital Goods tax class to use billing address.
	 *
	 * @param array       $location  Location.
	 * @param string|null $tax_class Tax class.
	 *
	 * @return array
	 */
	public function woocommerce_get_tax_location( $location, $tax_class ): array {
		if ( $tax_class && ! empty( WC()->customer ) && in_array( sanitize_title( $tax_class ), $this->settings->moss_tax_classes, true ) ) {
			if ( 'billing' === get_option( 'woocommerce_tax_based_on', 'billing' ) ) {
				return [ WC()->customer->get_billing_country(), WC()->customer->get_billing_state(), WC()->customer->get_billing_postcode(), WC()->customer->get_billing_city() ];
			} else {
				return [ WC()->customer->get_shipping_country(), WC()->customer->get_shipping_state(), WC()->customer->get_shipping_postcode(), WC()->customer->get_shipping_city() ];
			}
		}

		return $location;
	}

	/**
	 * @param array $fields
	 *
	 * @return array $fields
	 */
	public function add_required_classes( $fields ): array {
		$fields['billing'][ $this->vat_field_name ]['class'] = [
			'vat-number',
			'update_totals_on_change',
			'address-field',
			'form-row-wide',
		];

		return $fields;
	}


}
