<?php
$isset = $params ?: [];
$is_checked = $params['is_checked'];
?>
<p class="form-row vat_confirmation terms">
	<label for="vat_confirmation" class="checkbox">
		<input type="checkbox" class="input-checkbox" name="vat_confirmation" <?php checked( $is_checked, true ); ?> id="vat_confirmation" /> <span>
			<?php
			esc_html_e( 'I am already registered in VAT EU.', 'wp-woocommerce-eu-vat' );
			?></span>
	</label>
</p>
