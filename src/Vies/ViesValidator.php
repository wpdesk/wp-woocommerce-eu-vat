<?php
/**
 * Vies: Validator.
 *
 * @package WPDesk\WooCommerce\EUVAT\Vies;
 */

namespace WPDesk\WooCommerce\EUVAT\Vies;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use WPDesk\Persistence\ElementNotExistsException;
use WPDesk\Persistence\PersistentContainer;
use WPDesk\Persistence\Adapter\WordPress\WordpressTransientContainer;
use WPDesk\WooCommerce\EUVAT\Interfaces\ValidatorInterface;
use WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use WPDesk\WooCommerce\EUVAT\Vies\Exceptions\ViesCommunicationException;

/**
 * Validate VAT number in VIES database.
 * https://ec.europa.eu/taxation_customs/vies/#/vat-validation
 *
 * @package WPDesk\WooCommerce\EUVAT\Vies
 */
class ViesValidator implements ValidatorInterface, LoggerAwareInterface {

	use LoggerAwareTrait;

	/**
	 * Client.
	 *
	 * @var Client
	 */
	private $client;

	/**
	 * Shop settings.
	 *
	 * @var ShopSettings
	 */
	private $shop_settings;

	/**
	 * @var PersistentContainer
	 */
	private $persistence;

	/**
	 * @param ShopSettings    $shop_settings Shop settings.
	 * @param LoggerInterface $logger        Logger.
	 */
	public function __construct( ShopSettings $shop_settings, LoggerInterface $logger ) {
		$this->shop_settings = $shop_settings;
		$this->set_client( new Client() );
		$this->set_persistence( new WordpressTransientContainer( 'vies', 3600 ) );
		$this->setLogger( $logger );
	}

	/**
	 * Get persistence container.
	 *
	 * @return PersistentContainer
	 */
	public function get_persistence(): PersistentContainer {
		return $this->persistence;
	}

	/**
	 * Set persistence container.
	 *
	 * @param PersistentContainer $persistence Persistent.
	 */
	public function set_persistence( PersistentContainer $persistence ) {
		$this->persistence = $persistence;
	}

	/**
	 * Set client.
	 *
	 * @param Client $client Client.
	 */
	public function set_client( Client $client ) {
		$this->client = $client;
	}

	/**
	 * Get client.
	 *
	 * @return Client
	 */
	private function get_client(): Client {
		return $this->client;
	}

	/**
	 * Validate Vat Number in VIES database.
	 *
	 * @param string $vat_number Vat number.
	 * @param string $country    Country ISO.
	 *
	 * @return bool
	 * @throws ViesCommunicationException Throw exception if connection is fucked
	 */
	public function validate( $vat_number, $country ): bool {
		$transient_name = 'wpdesk_vies_validation_' . $country . $vat_number;
		try {
			return (bool) $this->get_persistence()->get( $transient_name );
		} catch ( ElementNotExistsException $e ) {
			try {
				$result   = $this->get_client()->checkVat( $country, $vat_number );
				$is_valid = $result->isValid();
				$this->get_persistence()->set( $transient_name, $is_valid );
			} catch ( ViesCommunicationException $e ) {
				$this->logger->error( $e->getMessage(), [ 'vat_number' => $vat_number, 'country' => $country ] );
				throw $e;
			}

			return $is_valid;
		}
	}
}
