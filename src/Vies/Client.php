<?php

namespace WPDesk\WooCommerce\EUVAT\Vies;

use SoapClient;
use SoapFault;
use WPDesk\WooCommerce\EUVAT\Vies\Exceptions\ViesCommunicationException;

/**
 * A client for the VIES SOAP web service
 */
class Client {

	/**
	 * URL to WSDL
	 *
	 * @var string
	 */
	protected $wsdl = 'https://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';

	/**
	 * SOAP client
	 *
	 * @var SoapClient
	 */
	protected $soapClient;

	/**
	 * Check VAT
	 *
	 * @param string $countryCode Country code
	 * @param string $vatNumber   VAT number
	 *
	 * @return Response
	 * @throws ViesCommunicationException
	 */
	public function checkVat( string $countryCode, string $vatNumber ): Response {
		try {
			return $this->getSoapClient()->checkVat(
				[
					'countryCode' => $countryCode,
					'vatNumber'   => $vatNumber,
				]
			);
		} catch ( SoapFault $e ) {
			throw new ViesCommunicationException( esc_html__( 'Error communicating with VIES service - please try again', 'wp-woocommerce-eu-vat' ), 0, $e );
		}
	}

	/**
	 * Get SOAP client
	 *
	 * @return SoapClient
	 * @throws SoapFault
	 */
	protected function getSoapClient(): SoapClient {
		if ( null === $this->soapClient ) {
			$this->soapClient = new SoapClient(
				$this->wsdl,
				[
					'classmap'   => [ 'checkVatResponse' => Response::class ],
					'user_agent' => 'Mozilla',
					'exceptions' => true,
				]
			);
		}

		return $this->soapClient;
	}
}
