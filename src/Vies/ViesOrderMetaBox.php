<?php

namespace WPDesk\WooCommerce\EUVAT\Vies;

use WPDesk\WooCommerce\EUVAT\Settings\Settings;
use WPDesk\WooCommerce\EUVAT\Settings\ShopSettings;
use WPDesk\PluginBuilder\Plugin\Hookable;
use WPDesk\View\Renderer\Renderer;
use Exception;
use WC_Order;
use WP_Post;

/**
 * Add VIES meta box to Order.
 *
 * @package WPDesk\WooCommerce\EUVAT\Vies
 */
class ViesOrderMetaBox implements Hookable {

	const META_BOX_ID = 'woocommerce_invoices_eu_vat';

	/**
	 * Settings.
	 *
	 * @var Settings
	 */
	private $settings;


	/**
	 * Shop settings.
	 *
	 * @var ShopSettings
	 */
	private $shop_settings;

	/**
	 * Renderer.
	 *
	 * @var Renderer
	 */
	private $renderer;

	/**
	 * @param Settings     $settings
	 * @param ShopSettings $shop_settings Shop settings.
	 * @param Renderer     $renderer      View renderer.
	 */
	public function __construct( Settings $settings, ShopSettings $shop_settings, Renderer $renderer ) {
		$this->settings      = $settings;
		$this->shop_settings = $shop_settings;
		$this->renderer      = $renderer;
	}

	/**
	 * Fires hooks.
	 */
	public function hooks() {
		add_action( 'add_meta_boxes', [ $this, 'add_meta_boxes_action' ], 30,2 );
	}

	/**
	 * Add Meta Boxes.
	 */
	public function add_meta_boxes_action( $order_page, $order ) {
		if( $order instanceof WC_Order ) {
			if( $order->get_billing_country() !== get_option( 'woocommerce_default_country') ) {
				add_meta_box( self::META_BOX_ID, __( 'EU VAT', 'wp-woocommerce-eu-vat' ), [ $this, 'render' ], [ 'shop_order', 'shop_subscription', 'woocommerce_page_wc-orders' ], 'side' );
			}
		}
	}

	/**
	 * @throws Exception
	 */
	public function get_order_for_meta_box( $object ): WC_Order {
		if ( $object instanceof WP_Post ) {
			$order = wc_get_order( $object->ID );
			if ( $order ) {
				return $order;
			}
		} elseif ( $object instanceof WC_Order ) {
			return $object;
		}

		throw new Exception( 'Unknown order object!' );
	}

	/**
	 * @param WP_Post|WC_Order|null $post_or_order_object
	 *
	 * @throws Exception
	 */
	public function render( $post_or_order_object ) {
		try {
			$order = $this->get_order_for_meta_box( $post_or_order_object );
			if ( ! $this->is_eu_order( $order ) ) {
				echo wpautop( esc_html__( 'This order is out of scope for EU VAT.', 'wp-woocommerce-eu-vat' ) ); //phpcs:ignore

				return;
			}

			$moss_meta_data = $this->get_order_vat_data( $order );
			$countries      = WC()->countries->get_countries();

			$params['vat_field_label'] = ! empty( $this->settings->vat_field_label ) ? $this->settings->vat_field_label : __( 'VAT ID', 'wp-woocommerce-eu-vat' );
			$params['countries']       = $countries;
			$params                    = array_merge( $params, $moss_meta_data );
			$html                      = $this->renderer->render( 'vies-order-metabox', $params );
			echo $html; //phpcs:ignore
		} catch ( Exception $e ) {
			// logger
		}

	}


	/**
	 * @param WC_Order $order The order object.
	 *
	 * @return array
	 */
	private function get_order_vat_data( WC_Order $order ): array {
		return array(
			'vat_number'                     => $order->get_meta( '_vat_number' ),
			'vat_number_self_declared'       => wc_string_to_bool( $order->get_meta( '_vat_number_self_declared' ) ),
			'valid'                          => wc_string_to_bool( $order->get_meta( '_vat_number_is_valid' ) ),
			'validated'                      => wc_string_to_bool( $order->get_meta( '_vat_number_is_validated' ) ),
			'billing_country'                => $order->get_billing_country(),
			'ip_address'                     => $order->get_customer_ip_address(),
			'ip_country'                     => $order->get_meta( '_customer_ip_country' ),
			'customer_self_declared_country' => wc_string_to_bool( $order->get_meta( '_customer_self_declared_country' ) ),
		);
	}

	/**
	 * @param WC_Order $order Order.
	 *
	 * @return bool
	 */
	protected function is_eu_order( WC_Order $order ) {
		return in_array( $order->get_billing_country(), $this->shop_settings->get_eu_countries() );
	}


}
