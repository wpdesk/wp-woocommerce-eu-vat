<?php
/**
 * Vies: Response DTO.
 *
 * @package WPDesk\WooCommerce\EUVAT\Vies;
 */
namespace WPDesk\WooCommerce\EUVAT\Vies;

/**
 * Response for mapping SOAP data.
 *
 * @package WPDesk\WooCommerce\EUVAT\Vies
 */
class Response {

	/**
	 * Country code.
	 *
	 * @var string
	 */
	protected $countryCode;

	/**
	 * Vat number.
	 *
	 * @var string
	 */
	protected $vatNumber;

	/**
	 * Request date.
	 *
	 * @var \DateTime
	 */
	protected $requestDate;

	/**
	 * Is valid.
	 *
	 * @var string
	 */
	protected $valid;

	/**
	 * Company name.
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * Company address.
	 *
	 * @var string
	 */
	protected $address;

	/**
	 * Get country code.
	 *
	 * @return string
	 */
	public function getCountryCode() {
		return $this->countryCode;
	}

	/**
	 * Get VAT number.
	 * @return string
	 */
	public function getVatNumber() {
		return $this->vatNumber;
	}

	/**
	 * Get request date.
	 *
	 * @return \DateTime
	 * @throws \Exception
	 */
	public function getRequestDate() {
		if ( ! $this->requestDate instanceof \DateTime ) {
			$this->requestDate = new \DateTime( $this->requestDate );
		}

		return $this->requestDate;
	}

	/**
	 * Is valid.
	 *
	 * @return mixed
	 */
	public function isValid() {
		return $this->valid;
	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Get address.
	 *
	 * @return string
	 */
	public function getAddress() {
		return $this->address;
	}
}
