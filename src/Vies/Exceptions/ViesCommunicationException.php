<?php

namespace WPDesk\WooCommerce\EUVAT\Vies\Exceptions;

/**
 * Throw an exception when a connection to the VIES is not established.
 *
 * @package WPDesk\WooCommerce\EUVAT\Vies\Exception
 */
class ViesCommunicationException extends \RuntimeException {

}


