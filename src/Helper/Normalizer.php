<?php

namespace WPDesk\WooCommerce\EUVAT\Helper;

/**
 * Normalize some data for VIES.
 */
class Normalizer {

	/**
	 * @param string $vat_number
	 * @param string $country
	 *
	 * @return string|string[]
	 */
	public function normalize_vat_number( string $vat_number, string $country ): string {
		$vat_number = preg_replace( '/[^a-zA-Z0-9]/', '', strtoupper( $vat_number ) );
		$prefix     = substr( $vat_number, 0, 2 );
		if ( strtoupper( $country ) === $prefix ) {
			$vat_number = substr( $vat_number, 2 );
		}

		return $vat_number;
	}

	/**
	 * @param string $country Country ISO.
	 *
	 * @return string
	 */
	public function normalize_country_prefix( string $country ): string {
		$country = strtoupper( trim( $country ) );
		switch ( $country ) {
			case 'GR':
				$vat_prefix = 'EL';
				break;
			case 'IM':
				$vat_prefix = 'GB';
				break;
			case 'MC':
				$vat_prefix = 'FR';
				break;
			default:
				$vat_prefix = $country;
				break;
		}

		return $vat_prefix;
	}

}
