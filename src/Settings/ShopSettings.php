<?php

namespace WPDesk\WooCommerce\EUVAT\Settings;

use WC_Geolocation;

/**
 * Shop settings.
 *
 * @package WPDesk\WooCommerce\EUVAT\Settings
 */
class ShopSettings {

	/**
	 * @var string
	 */
	private $ip_country = '';

	/**
	 * Is EU VAT Number enabled?
	 *
	 * @return bool
	 */
	public function is_eu_vat_plugins_enabled(): bool {
		$active_plugins    = get_option( 'active_plugins' );
		$eu_vat_number     = 'woocommerce-eu-vat-number/woocommerce-eu-vat-number.php';
		$eu_vat_compliance = 'woocommerce-eu-vat-compliance-premium/eu-vat-compliance-premium.php';
		if ( in_array( $eu_vat_number, $active_plugins ) || in_array( $eu_vat_compliance, $active_plugins ) ) {
			return true;
		}

		return false;
	}

	/**
	 * @param string $customer_country
	 *
	 * @return bool
	 */
	public function is_customer_from_base_country( string $customer_country ): bool {
		return strtoupper( $customer_country ) === strtoupper( get_option( 'woocommerce_default_country' ) );
	}

	/**
	 * Get IP address country for user.
	 *
	 * @return string
	 */
	public function get_ip_country(): string {
		if ( ! $this->ip_country ) {
			$ip               = WC_Geolocation::geolocate_ip();
			$this->ip_country = $ip['country'];
		}

		return $this->ip_country;
	}

	/**
	 * Get EU Countries.
	 *
	 * @return array
	 */
	public function get_eu_countries(): array {
		$countries = WC()->countries->get_european_union_countries();
		if ( isset( $countries['GB'] ) ) {
			unset( $countries['GB'] );
		}
		$countries[] = 'EL';
		return $countries;
	}

}
