## [3.1.2] - 2024-07-19
### Fixed
- Greek fix

## [3.1.1] - 2024-04-02
### Fixed
- Translations

## [3.1.0] - 2024-03-18
### Fixed
- Refactor

## [3.0.0] - 2024-02-19
### Fixed
- Refactor

## [2.3.0] - 2023-06-19
### Added
- Logger to validator

## [2.2.1] - 2021-12-16
### Fixed
- Libraries update
- Fixed notice

## [2.2.0] - 2021-07-28
### Added
- OSS Support

## [2.1.4] - 2021-06-24
### Fixed
- Translations

## [2.1.3] - 2021-04-14
### Fixed
- Translations

## [2.1.2] - 2021-04-14
### Fixed
- Fixed url to WSDL

## [2.1.1] - 2021-01-21
### Fixed
- Fixed VAT number

## [2.1.0] - 2021-01-11
### Fixed
- Drop support for UK

## [2.0.1] - 2020-10-22
### Fixed
- Fixed translation

## [2.0.0] - 2020-10-22
### Fixed
- Fixed validator.
## Update
- Update persistence library

## [1.1.5] - 2020-05-12
### Fixed
- Fixed EU countries function.

## [1.1.4] - 2020-05-06
### Fixed
- Remove validation EU VAT from base country
- Fixed VAT field priority

## [1.1.3] - 2020-04-27
### Fixed
- WooCommerce 4.0
- Vat field priority

## [1.1.2] - 2020-01-23
### Fixed
- Added tax condition

## [1.1.1] - 2020-01-15
### Fixed
- Added documentation link

## [1.1.0] - 2020-01-15
### Fixed
- Added interface for validator, new methods, arguments for notice, fixed translations

## [1.0.1] - 2019-12-23
### Fixed
- Change property visibility

## [1.0.1] - 2019-12-19
### Added
- Added translations

## [1.0.0] - 2019-12-19
### Added
- First version
